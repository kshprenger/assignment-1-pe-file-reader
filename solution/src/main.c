///@file
///@brief Main file
#include "../include/main.h"
#include <stddef.h>
#include <stdio.h>
/// Application name string
#define APP_NAME "section-extractor"

/// @brief Print usage test
/// @param[in] f File to print to (e.g., stdout)
void usage(FILE *f)
{
  fprintf(f, "Usage: " APP_NAME " <in_file> <section_name> <out_file>\n");
}

/// @brief Application entry point
/// @param[in] argc Number of command line arguments
/// @param[in] argv Command line arguments
/// @return 0 in case of success or error code
int main(int argc, char **argv)
{
  if (argc < 3)
  {
    return 1;
  }
  char *in = argv[1];
  char *out = argv[3];
  char *name = argv[2];

  struct PeFile pe;
  FILE *f_in;
  FILE *f_out;

  open_file(&f_in, in, "r");
  open_file(&f_out, out, "w");

  pe.type = checkPE32Type(f_in);

  if (pe.type)
  {
    pe.pe32 = malloc(sizeof(struct PE32));
    if (createPE32(pe.pe32, f_in) != OK)
    {
      close_file(f_in);
      close_file(f_out);
      destroy_pe(&pe);
      return 1;
    }
  }

  else
  {
    pe.pe32_p = malloc(sizeof(struct PE32_p));
    if (createPE32_p(pe.pe32_p, f_in) != OK)
    {
      close_file(f_in);
      close_file(f_out);
      destroy_pe(&pe);
      return 1;
    }
  }

  if (write_section(name, &pe, &f_out, &f_in) != WRITE_OK)
  {
    close_file(f_in);
    close_file(f_out);
    destroy_pe(&pe);
    return 1;
  }
  close_file(f_in);
  close_file(f_out);
  destroy_pe(&pe);
  return 0;
}
