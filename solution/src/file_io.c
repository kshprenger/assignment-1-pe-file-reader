///@file
///@brief IO module file

#ifndef FILE_IO
#define FILE_IO
#include "../include/file_io/file_close.h"
#include "../include/file_io/file_open.h"

/// @brief Openning file
/// @param[in] file Pointer to file pointer needed to be open
/// @param[in] pathname system path name of the file
/// @param[in] mode mode of ope
/// @return OPEN_FAIL - if file open fails,\n OPEN_SUCCESS on success

enum open_file_status open_file(FILE **file, const char *pathname, const char *mode)
{
    *file = fopen(pathname, mode);
    return *file == NULL ? OPEN_FAIL : OPEN_SUCCESS;
}

/// @brief Closing file
/// @param[in] file Pointer to file needed to be close
/// @return CLOSE_FAIL - if file open fails,\n CLOSE_SUCCESS on success
enum close_file_status close_file(FILE *file)
{
    return fclose(file) == 0 ? CLOSE_SUCCESS : CLOSE_FAIL;
}

#endif
