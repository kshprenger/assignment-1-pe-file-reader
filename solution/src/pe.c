///@file
///@brief PE functions module file

#ifndef PE
#define PE

#define MAIN_OFFSET 0x3c

#include "../include/pe/pe.h"
#include "../include/file_io/file_in.h"
#include "../include/file_io/file_out.h"
#include <stdio.h>
#include <stdlib.h>

/// @brief Comparing string function
/// @param[in] s1 First string
/// @param[in] s2 Second string
/// @return true if and only if strings are equal or false

bool cmp_string(const char *s1, const char *s2)
{
    for (size_t i = 0; i < strlen(s1) && i < strlen(s2); i++)
    {
        if (s1[i] != s2[i])
        {
            return false;
        }
    }
    return true;
}

/// @brief Creating PE file in format pe32 -> reading + allocating memory
/// @param[in] pe Pointer to pe32
/// @param[in] f File for data read
/// @return OK - success, AllOC_ERROR - error while allocating memory, READ_ERROR - error while reading data from f

enum in_status createPE32(struct PE32 *pe, FILE *f)
{
    uint32_t offset;
    pe->coff_header = malloc(sizeof(struct COFFHeader));
    pe->std_fields = malloc(sizeof(struct StdFields));
    pe->windows_spec = malloc(sizeof(struct WindowsSpec));
    pe->data_dir = malloc(sizeof(struct DataDir));

    if (pe->coff_header == NULL || pe->std_fields == NULL || pe->windows_spec == NULL || pe->data_dir == NULL)
    {
        return ALLOC_ERROR;
    }

    fseek(f, MAIN_OFFSET, SEEK_SET);
    fread(&offset, 4, 1, f);
    fseek(f, offset, SEEK_SET);
    if (fread(pe->coff_header, sizeof(struct COFFHeader), 1, f) != 1)
    {
        return READ_ERROR;
    }
    if (fread(pe->std_fields, sizeof(struct StdFields), 1, f) != 1)
    {
        return READ_ERROR;
    }
    if (fread(pe->windows_spec, sizeof(struct WindowsSpec), 1, f) != 1)
    {
        return READ_ERROR;
    }
    if (fread(pe->data_dir, sizeof(struct DataDir), 1, f) != 1)
    {
        return READ_ERROR;
    }

    pe->sections = malloc((pe->coff_header->NumberOfSections) * sizeof(struct SectionTable *));
    if (pe->sections == NULL)
    {
        return ALLOC_ERROR;
    }
    for (size_t i = 0; i < pe->coff_header->NumberOfSections; i++)
    {
        pe->sections[i] = malloc(sizeof(struct SectionTable));

        if (pe->sections[i] == NULL)
        {
            return ALLOC_ERROR;
        }

        if (fread(pe->sections[i], sizeof(struct SectionTable), 1, f) != 1)
        {
            return READ_ERROR;
        }
    }

    return OK;
}

/// @brief Creating PE file in format pe32+ -> reading + allocating memory
/// @param[in] pe Pointer to pe32+
/// @param[in] f File for data read
/// @return OK - success, AllOC_ERROR - error while allocating memory,\n READ_ERROR - error while reading data from f

enum in_status createPE32_p(struct PE32_p *pe, FILE *f)
{
    uint32_t offset;
    pe->coff_header = malloc(sizeof(struct COFFHeader));
    pe->std_fields = malloc(sizeof(struct StdFields_p));
    pe->windows_spec = malloc(sizeof(struct WindowsSpec_p));
    pe->data_dir = malloc(sizeof(struct DataDir_p));

    if (pe->coff_header == NULL || pe->std_fields == NULL || pe->windows_spec == NULL || pe->data_dir == NULL)
    {
        return ALLOC_ERROR;
    }

    fseek(f, MAIN_OFFSET, SEEK_SET);
    fread(&offset, 4, 1, f);
    fseek(f, offset, SEEK_SET);
    if (fread(pe->coff_header, sizeof(struct COFFHeader), 1, f) != 1)
    {
        return READ_ERROR;
    }
    if (fread(pe->std_fields, sizeof(struct StdFields_p), 1, f) != 1)
    {
        return READ_ERROR;
    }
    if (fread(pe->windows_spec, sizeof(struct WindowsSpec_p), 1, f) != 1)
    {
        return READ_ERROR;
    }
    if (fread(pe->data_dir, sizeof(struct DataDir_p), 1, f) != 1)
    {
        return READ_ERROR;
    }
    pe->sections = malloc((pe->coff_header->NumberOfSections) * sizeof(struct SectionTable *));
    if (pe->sections == NULL)
    {
        return ALLOC_ERROR;
    }
    for (size_t i = 0; i < pe->coff_header->NumberOfSections; i++)
    {
        pe->sections[i] = malloc(sizeof(struct SectionTable));

        if (pe->sections[i] == NULL)
        {
            return ALLOC_ERROR;
        }

        if (fread(pe->sections[i], sizeof(struct SectionTable), 1, f) != 1)
        {
            return READ_ERROR;
        }
    }
    return OK;
}

/// @brief Checking pe signature function -> pe32 or pe32+
/// @param[in] f File for data read
/// @return true - signature is 0x10b -> pe32,\n false - signature is 0x20b -> pe32+
bool checkPE32Type(FILE *f)
{
    uint32_t offset;
    fseek(f, MAIN_OFFSET, SEEK_SET);
    fread(&offset, 4, 1, f);
    fseek(f, offset + 24, SEEK_SET);
    uint16_t magic;
    fread(&magic, 2, 1, f);
    if (magic == 0x20b)
        return false;
    return true;
}

/// @brief Destroying pe file by deallocating memory
/// @param[in] pe Pe File

void destroy_pe(struct PeFile *pe)
{
    if (pe->type)
    {
        for (size_t i = 0; i < pe->pe32->coff_header->NumberOfSections; i++)
        {
            free(pe->pe32->sections[i]);
        }
        free(pe->pe32->sections);
        free(pe->pe32->coff_header);
        free(pe->pe32->data_dir);
        free(pe->pe32->windows_spec);
        free(pe->pe32->std_fields);
        free(pe->pe32);
    }
    else
    {
        for (size_t i = 0; i < pe->pe32_p->coff_header->NumberOfSections; i++)
        {
            free(pe->pe32_p->sections[i]);
        }
        free(pe->pe32_p->sections);
        free(pe->pe32_p->coff_header);
        free(pe->pe32_p->data_dir);
        free(pe->pe32_p->windows_spec);
        free(pe->pe32_p->std_fields);
        free(pe->pe32_p);
    }
}

/// @brief Writing certain pe section to output file
/// @param[in] section Name of needed section
/// @param[in] pe Pe File
/// @param[in] f_out File for data write
/// @param[in] f_in File for data read
/// @return WRITE_ERROR - if writing error accures or section is not found,\n else - WRITE_OK

enum write_status write_section(char *section, struct PeFile *pe, FILE **f_out, FILE **f_in)
{
    if (pe->type)
    {
        for (size_t i = 0; i < pe->pe32->coff_header->NumberOfSections; i++)
        {
            if (cmp_string(pe->pe32->sections[i]->Name, section))
            {
                fseek(*f_in, pe->pe32->sections[i]->PointerToRawData, SEEK_SET);
                char d;
                for (size_t j = 0; j < pe->pe32->sections[i]->SizeOfRawData; j++)
                {
                    if (fread(&d, 1, 1, *f_in) != 1)
                    {
                        return WRITE_ERROR;
                    }
                    if (fwrite(&d, 1, 1, *f_out) != 1)
                    {
                        return WRITE_ERROR;
                    }
                }
            }
        }
    }
    else
    {
        for (size_t i = 0; i < pe->pe32_p->coff_header->NumberOfSections; i++)
        {
            if (cmp_string(pe->pe32_p->sections[i]->Name, section))
            {
                fseek(*f_in, pe->pe32_p->sections[i]->PointerToRawData, SEEK_SET);
                char d;
                for (int64_t j = 0; j < pe->pe32_p->sections[i]->SizeOfRawData; j++)
                {
                    if (fread(&d, 1, 1, *f_in) != 1)
                    {
                        return WRITE_ERROR;
                    }
                    if (fwrite(&d, 1, 1, *f_out) != 1)
                    {
                        return WRITE_ERROR;
                    }
                }
                return WRITE_OK;
            }
        }
    }
    return WRITE_ERROR;
}

#endif
