///@file
///@brief Writing operation header 

#ifndef FILE_OUT_HEADER
#define FILE_OUT_HEADER
#include <stdio.h>
enum write_status
{
    WRITE_OK = 0,
    WRITE_ERROR
};

enum write_status write_section(char *section, struct PeFile *pe, FILE **f_out, FILE **f_in);

#endif
