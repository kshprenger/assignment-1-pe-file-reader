///@file
///@brief Reading and constructing pe header

#ifndef FILE_IN_HEADER
#define FILE_IN_HEADER
#include "../pe/pe.h"
#include <stdio.h>

enum in_status
{
    OK = 0,
    READ_ERROR,
    ALLOC_ERROR,
};

enum in_status createPE32(struct PE32 *pe, FILE *f);

enum in_status createPE32_p(struct PE32_p *pe, FILE *f);

#endif
