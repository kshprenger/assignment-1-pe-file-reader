///@file
///@brief Opening operation header

#ifndef FILE_OPEN_HEADER
#define FILE_OPEN_HEADER
#include <stdio.h>

enum open_file_status
{
    OPEN_SUCCESS = 0,
    OPEN_FAIL
};

enum open_file_status open_file(FILE **file, const char *pathname, const char *mode);

#endif
