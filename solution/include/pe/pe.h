/// @file
/// @brief PE general structs
#ifndef PE_HEADER
#define PE_HEADER
#define PACKED __attribute__((packed))
#include "./pe32+.h"
#include "./pe32.h"
#include <stdbool.h>
#include <string.h>

/*! \struct PeFile
    \brief Generic Pe FIle struct
    \param type type of pe -> pe32 or pe32+
    \param pe32 pointer to p32 format file if type
    \param pe32+ pointer to pe32+
*/
struct PeFile
{
    bool type;
    struct PE32 *pe32;
    struct PE32_p *pe32_p;
};

/*! \struct COFFHeader
    \brief Standard pe coff header format struct
    \nFor more info:\n
    https://learn.microsoft.com/en-us/windows/win32/debug/pe-format#coff-file-header-object-and-image
 */
struct
#if defined _MSC_VER
#pragma pack(push, 1)
#else
    PACKED
#endif
    COFFHeader
{
    uint8_t magic[4];
    uint16_t Machine;
    uint16_t NumberOfSections;
    uint32_t TimeDateStamp;
    uint32_t PointerToSymbolTable;
    uint32_t NumberOfSymbols;
    uint16_t SizeOfOptionalHeader;
    uint16_t Characteristics;
};

/*! \struct SectionTable
    \brief Standard pe section table struct
    \nFor more info:\n
    https://learn.microsoft.com/en-us/windows/win32/debug/pe-format
 */
struct
#if defined _MSC_VER
#pragma pack(push, 1)
#else
    PACKED
#endif
    SectionTable
{
    char Name[8];
    uint32_t VirtualSize;
    uint32_t VirtualAddress;
    uint32_t SizeOfRawData;
    uint32_t PointerToRawData;
    uint32_t PointerToRelocations;
    uint32_t PointerToLinenumbers;
    uint16_t NumberOfRelocations;
    uint16_t NumberOfLinenumbers;
    uint32_t Characteristics;
};
void destroy_pe(struct PeFile *pe);
bool checkPE32Type(FILE *f);
#endif
