///@file
///@brief PE32 specific structs

#ifndef PE32_HEADER
#define PE32_HEADER
#include <inttypes.h>
#include <stdio.h>

/*! \struct PE32
    \brief Struct htat contains whole pe32 header
    \param coff_header pointer to COFF header
    \param std_fields pointer to p32 specific Standard Fields header
    \param windows_spec pointer to pe32 specific Windows Specific Fields header
    \param data_dir pointer to Data Directory header
    \param sections pointer to array of section headers
*/

struct PE32
{
    struct COFFHeader *coff_header;
    struct StdFields *std_fields;
    struct WindowsSpec *windows_spec;
    struct DataDir *data_dir;
    struct SectionTable **sections;
};

/*! \struct StdFields
    \brief PE32 specific Standard Fields struct
    \nFor more info:\n
    https://learn.microsoft.com/en-us/windows/win32/debug/pe-format#optional-header-standard-fields-image-only
*/

struct
#if defined _MSC_VER
#pragma pack(push, 1)
#else
    PACKED
#endif
    StdFields
{
    uint16_t Magic;
    uint8_t MajorLinkerVersion;
    uint8_t MinorLinkerVersion;
    uint32_t SizeOfCode;
    uint32_t SizeOfInitializedData;
    uint32_t SizeOfUninitializedData;
    uint32_t AddressOfEntryPoint;
    uint32_t BaseOfCode;
    uint32_t BaseOfData;
};

/*! \struct WindowsSpec
    \brief PE32 specific Windows Specific Fields struct
    \nFor more info:\n
    https://learn.microsoft.com/en-us/windows/win32/debug/pe-format#optional-header-windows-specific-fields-image-only
*/

struct
#if defined _MSC_VER
#pragma pack(push, 1)
#else
    PACKED
#endif
    WindowsSpec
{
    uint32_t ImageBase;
    uint32_t SectionAlignment;
    uint32_t FileAlignment;
    uint16_t MajorOperatingSystemVersion;
    uint16_t MinorOperatingSystemVersion;
    uint16_t MajorImageVersion;
    uint16_t MinorImageVersion;
    uint16_t MajorSubsystemVersion;
    uint16_t MinorSubsystemVersion;
    uint32_t Win32VersionValue;
    uint32_t SizeOfImage;
    uint32_t SizeOfHeaders;
    uint32_t CheckSum;
    uint16_t Subsystem;
    uint16_t DllCharacteristics;
    uint32_t SizeOfStackReserve;
    uint32_t SizeOfStackCommit;
    uint32_t SizeOfHeapReserve;
    uint32_t SizeOfHeapCommit;
    uint32_t LoaderFlags;
    uint32_t NumberOfRvaAndSizes;
};

/*! \struct DataDir
    \brief PE32 specific Data Directory struct
    \nFor more info:\n
    https://learn.microsoft.com/en-us/windows/win32/debug/pe-format#optional-header-data-directories-image-only
*/

struct
#if defined _MSC_VER
#pragma pack(push, 1)
#else
    PACKED
#endif
    DataDir
{
    uint64_t Export;
    uint64_t Import;
    uint64_t Resource;
    uint64_t Exception;
    uint64_t Certificate;
    uint64_t Relocation;
    uint64_t Debug;
    uint64_t Architecture;
    uint64_t Global;
    uint64_t TLS;
    uint64_t Config;
    uint64_t Bound;
    uint64_t IAT;
    uint64_t Delay;
    uint64_t CLR;
    uint64_t Reserved;
};

#endif
